/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLEventEndpointVenue.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   eventEndpoint/v1
// Description:
//   This is an API
// Classes:
//   GTLEventEndpointVenue (0 custom class methods, 4 custom properties)

#import "GTLEventEndpointVenue.h"

// ----------------------------------------------------------------------------
//
//   GTLEventEndpointVenue
//

@implementation GTLEventEndpointVenue
@dynamic identifier, lat, lng, venueName;

+ (NSDictionary *)propertyToJSONKeyMap {
  NSDictionary *map = @{
    @"identifier" : @"id"
  };
  return map;
}

@end
