/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLCityEndpointCommonResponse.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   cityEndpoint/v1
// Description:
//   This is an API
// Classes:
//   GTLCityEndpointCommonResponse (0 custom class methods, 6 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLObject.h"
#else
  #import "GTLObject.h"
#endif

// ----------------------------------------------------------------------------
//
//   GTLCityEndpointCommonResponse
//

@interface GTLCityEndpointCommonResponse : GTLObject
@property (nonatomic, retain) NSNumber *errorId;  // intValue
@property (nonatomic, copy) NSString *errorMessage;

// identifier property maps to 'id' in JSON (to avoid Objective C's 'id').
@property (nonatomic, retain) NSNumber *identifier;  // longLongValue

@property (nonatomic, copy) NSString *sId;
@property (nonatomic, retain) NSNumber *status;  // boolValue
@property (nonatomic, retain) NSNumber *uid;  // longLongValue
@end
