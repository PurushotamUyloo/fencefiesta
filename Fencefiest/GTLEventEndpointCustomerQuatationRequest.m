/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLEventEndpointCustomerQuatationRequest.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   eventEndpoint/v1
// Description:
//   This is an API
// Classes:
//   GTLEventEndpointCustomerQuatationRequest (0 custom class methods, 11 custom properties)

#import "GTLEventEndpointCustomerQuatationRequest.h"

#import "GTLEventEndpointAddtionRequirement.h"
#import "GTLEventEndpointImage.h"
#import "GTLEventEndpointVenue.h"

// ----------------------------------------------------------------------------
//
//   GTLEventEndpointCustomerQuatationRequest
//

@implementation GTLEventEndpointCustomerQuatationRequest
@dynamic addtionalNote, addtionRequirementList, eventDate, eventId, identifier,
         imagesList, maxBudget, maxGathering, minBudget, minGathering, venue;

+ (NSDictionary *)propertyToJSONKeyMap {
  NSDictionary *map = @{
    @"identifier" : @"id"
  };
  return map;
}

+ (NSDictionary *)arrayPropertyToClassMap {
  NSDictionary *map = @{
    @"addtionRequirementList" : [GTLEventEndpointAddtionRequirement class],
    @"imagesList" : [GTLEventEndpointImage class]
  };
  return map;
}

@end
