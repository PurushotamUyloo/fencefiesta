/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLEventEndpointConstants.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   eventEndpoint/v1
// Description:
//   This is an API

#import <Foundation/Foundation.h>

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLDefines.h"
#else
  #import "GTLDefines.h"
#endif

// Authorization scope
// View your email address
GTL_EXTERN NSString * const kGTLAuthScopeEventEndpointUserinfoEmail;  // "https://www.googleapis.com/auth/userinfo.email"

// GTLEventEndpoint - EventStatus
GTL_EXTERN NSString * const kGTLEventEndpoint_EventStatus_Active;  // "Active"
GTL_EXTERN NSString * const kGTLEventEndpoint_EventStatus_Deactivated;  // "Deactivated"
GTL_EXTERN NSString * const kGTLEventEndpoint_EventStatus_Deleted;  // "Deleted"
GTL_EXTERN NSString * const kGTLEventEndpoint_EventStatus_ImageUploaded;  // "ImageUploaded"
GTL_EXTERN NSString * const kGTLEventEndpoint_EventStatus_ImageVerificationFailed;  // "ImageVerificationFailed"
GTL_EXTERN NSString * const kGTLEventEndpoint_EventStatus_ImageVerified;  // "ImageVerified"
GTL_EXTERN NSString * const kGTLEventEndpoint_EventStatus_Registered;  // "Registered"

// GTLQueryEventEndpoint - EventStatus
GTL_EXTERN NSString * const kGTLEventEndpointEventStatusActive;  // "Active"
GTL_EXTERN NSString * const kGTLEventEndpointEventStatusDeactivated;  // "Deactivated"
GTL_EXTERN NSString * const kGTLEventEndpointEventStatusDeleted;  // "Deleted"
GTL_EXTERN NSString * const kGTLEventEndpointEventStatusImageUploaded;  // "ImageUploaded"
GTL_EXTERN NSString * const kGTLEventEndpointEventStatusImageVerificationFailed;  // "ImageVerificationFailed"
GTL_EXTERN NSString * const kGTLEventEndpointEventStatusImageVerified;  // "ImageVerified"
GTL_EXTERN NSString * const kGTLEventEndpointEventStatusRegistered;  // "Registered"
