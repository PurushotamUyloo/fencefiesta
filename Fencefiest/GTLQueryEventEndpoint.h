/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLQueryEventEndpoint.h
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   eventEndpoint/v1
// Description:
//   This is an API
// Classes:
//   GTLQueryEventEndpoint (5 custom class methods, 9 custom properties)

#if GTL_BUILT_AS_FRAMEWORK
  #import "GTL/GTLQuery.h"
#else
  #import "GTLQuery.h"
#endif

@class GTLEventEndpointBasePackages;
@class GTLEventEndpointGallery;

@interface GTLQueryEventEndpoint : GTLQuery

//
// Parameters valid on all methods.
//

// Selector specifying which fields to include in a partial response.
@property (nonatomic, copy) NSString *fields;

//
// Method-specific parameters; see the comments below for more information.
//
@property (nonatomic, copy) NSString *createdBy;
// Remapped to 'descriptionProperty' to avoid NSObject's 'description'.
@property (nonatomic, copy) NSString *descriptionProperty;
@property (nonatomic, assign) long long eId;
@property (nonatomic, assign) long long eventId;
@property (nonatomic, copy) NSString *eventName;
@property (nonatomic, copy) NSString *eventStatus;
@property (nonatomic, copy) NSString *remarks;
@property (nonatomic, assign) long long updatedBy;

#pragma mark - Service level methods
// These create a GTLQueryEventEndpoint object.

// Method: eventEndpoint.getEventList
//  Required:
//   eventStatus:
//      kGTLEventEndpointEventStatusActive: "Active"
//      kGTLEventEndpointEventStatusDeactivated: "Deactivated"
//      kGTLEventEndpointEventStatusDeleted: "Deleted"
//      kGTLEventEndpointEventStatusImageUploaded: "ImageUploaded"
//      kGTLEventEndpointEventStatusImageVerificationFailed: "ImageVerificationFailed"
//      kGTLEventEndpointEventStatusImageVerified: "ImageVerified"
//      kGTLEventEndpointEventStatusRegistered: "Registered"
//  Authorization scope(s):
//   kGTLAuthScopeEventEndpointUserinfoEmail
// Fetches a GTLEventEndpointEventCollection.
+ (instancetype)queryForGetEventListWithEventStatus:(NSString *)eventStatus;

// Method: eventEndpoint.registerEvent
//  Authorization scope(s):
//   kGTLAuthScopeEventEndpointUserinfoEmail
// Fetches a GTLEventEndpointCommonResponse.
+ (instancetype)queryForRegisterEventWithEventName:(NSString *)eventName
                               descriptionProperty:(NSString *)descriptionProperty
                                         createdBy:(NSString *)createdBy;

// Method: eventEndpoint.updateEventBasePackages
//  Authorization scope(s):
//   kGTLAuthScopeEventEndpointUserinfoEmail
// Fetches a GTLEventEndpointCommonResponse.
+ (instancetype)queryForUpdateEventBasePackagesWithObject:(GTLEventEndpointBasePackages *)object
                                                      eId:(long long)eId
                                                eventName:(NSString *)eventName;

// Method: eventEndpoint.updateEventStatus
//  Required:
//   eventStatus:
//      kGTLEventEndpointEventStatusActive: "Active"
//      kGTLEventEndpointEventStatusDeactivated: "Deactivated"
//      kGTLEventEndpointEventStatusDeleted: "Deleted"
//      kGTLEventEndpointEventStatusImageUploaded: "ImageUploaded"
//      kGTLEventEndpointEventStatusImageVerificationFailed: "ImageVerificationFailed"
//      kGTLEventEndpointEventStatusImageVerified: "ImageVerified"
//      kGTLEventEndpointEventStatusRegistered: "Registered"
//  Authorization scope(s):
//   kGTLAuthScopeEventEndpointUserinfoEmail
// Fetches a GTLEventEndpointCommonResponse.
+ (instancetype)queryForUpdateEventStatusWithEventId:(long long)eventId
                                         eventStatus:(NSString *)eventStatus
                                           updatedBy:(long long)updatedBy
                                             remarks:(NSString *)remarks;

// Method: eventEndpoint.updateGallery
//  Authorization scope(s):
//   kGTLAuthScopeEventEndpointUserinfoEmail
// Fetches a GTLEventEndpointCommonResponse.
+ (instancetype)queryForUpdateGalleryWithObject:(GTLEventEndpointGallery *)object
                                        eventId:(long long)eventId;

@end
