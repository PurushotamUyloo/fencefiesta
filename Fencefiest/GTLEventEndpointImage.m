/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLEventEndpointImage.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   eventEndpoint/v1
// Description:
//   This is an API
// Classes:
//   GTLEventEndpointImage (0 custom class methods, 6 custom properties)

#import "GTLEventEndpointImage.h"

// ----------------------------------------------------------------------------
//
//   GTLEventEndpointImage
//

@implementation GTLEventEndpointImage
@dynamic descriptionProperty, identifier, name, sURL, tURL, url;

+ (NSDictionary *)propertyToJSONKeyMap {
  NSDictionary *map = @{
    @"descriptionProperty" : @"description",
    @"identifier" : @"id"
  };
  return map;
}

@end
