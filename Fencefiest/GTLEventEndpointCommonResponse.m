/* This file was generated by the ServiceGenerator.
 * The ServiceGenerator is Copyright (c) 2017 Google Inc.
 */

//
//  GTLEventEndpointCommonResponse.m
//

// ----------------------------------------------------------------------------
// NOTE: This file is generated from Google APIs Discovery Service.
// Service:
//   eventEndpoint/v1
// Description:
//   This is an API
// Classes:
//   GTLEventEndpointCommonResponse (0 custom class methods, 6 custom properties)

#import "GTLEventEndpointCommonResponse.h"

// ----------------------------------------------------------------------------
//
//   GTLEventEndpointCommonResponse
//

@implementation GTLEventEndpointCommonResponse
@dynamic errorId, errorMessage, identifier, sId, status, uid;

+ (NSDictionary *)propertyToJSONKeyMap {
  NSDictionary *map = @{
    @"identifier" : @"id"
  };
  return map;
}

@end
